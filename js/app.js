'use strict';

/* Smooth scroll to anchors */
$(document).ready(function () {

    $('a').on('click', function (event) {
        if (this.hash !== '') {
            event.preventDefault();
            const hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1200, function() {
                window.location.hash = hash;
            });
        }
    });
});

/* Scroll to top button */
const btn = $('#scroll-top__container');

$(window).scroll(function() {
    if ($(window).scrollTop() > 1000) {
        btn.addClass('show');
    } else {
        btn.removeClass('show')
    }
});

btn.on('click', function(event) {
    event.preventDefault();
    $('html, body').animate({
        scrollTop: 0
    }, 1200);
});

/* slideToggle section */
$(document).ready(function() {
    $('.slide-up__label').click(function() {
    $('.popular-posts').slideToggle(1200);
    });
});

/* swap text on slideToggle effect */
$(document).ready(function() {
    $('.slide-up__label__text').click(function() {
        $(this).text($(this).text() ===  'Show this section' ? 'Hide section above'
            : 'Show this section'
        );
    });
});